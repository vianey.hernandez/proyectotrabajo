 import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';  
import { FooterComponent } from './sharedpages/footer/footer.component'; 
import { Nav2Component } from './sharedpages/nav2/nav2.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CatalogoMEBRComponent } from './pages/catalogo-mebr/catalogo-mebr.component'; 
import { TableComponent } from './components/table/table.component'; 
import {HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatInputModule} from '@angular/material/input'; 
import {MatButtonModule} from '@angular/material/button';
import {MatTableModule} from '@angular/material/table';	
import { MatTableExporterModule } from 'mat-table-exporter';
import {CdkTableModule} from '@angular/cdk/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import { PopUpComponent } from './components/pop-up/pop-up.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,   
    FooterComponent, 
    Nav2Component, 
    HomePageComponent,
    TableComponent,
    CatalogoMEBRComponent,
    PopUpComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatTableExporterModule,
    CdkTableModule,
    MatTabsModule,
    MatDialogModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
