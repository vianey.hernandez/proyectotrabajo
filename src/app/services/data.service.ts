import { Injectable } from '@angular/core';
import { ValidateData } from '../validate-data';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  private apiUrl = 'http://localhost:5000/catalogos'

  constructor(private http:HttpClient) { }
  
  getData(): Observable<ValidateData[]> {
    return this.http.get<ValidateData[]>(this.apiUrl);
  } 

  getCatalogo(id: number): Observable<ValidateData[]> {
    const url = `${this.apiUrl}/${id}`; 
    return this.http.get<ValidateData[]>(url);
  }

}

