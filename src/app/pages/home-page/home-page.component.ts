import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.sass']
})
export class HomePageComponent implements OnInit {
  usuario={
    name:"Vianey Hernández Aguilar",
    edad:23,
    rol:"PRUEBA_500x$"
  }
  constructor() { }

  ngOnInit(): void {
  }

}
