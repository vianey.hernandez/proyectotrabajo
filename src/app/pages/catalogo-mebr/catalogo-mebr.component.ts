import { DataService} from './../../services/data.service';
import { Component, OnInit, OnDestroy} from '@angular/core';
import { ValidateData } from '../../validate-data';
import { Subscription,Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-catalogo-mebr',
  templateUrl: './catalogo-mebr.component.html',
  styleUrls: ['./catalogo-mebr.component.sass'],
})
export class CatalogoMEBRComponent implements OnInit, OnDestroy{
  datitos: ValidateData[] = [];
  //crear un objeto de subscripción al observable
  subs = new Subscription();
  dataArray: any; //variable auxiliar
  size: any; // tamaño del Array JSON

  constructor(private financeService: DataService) {}

  //destruir la subscripcion
  ngOnDestroy(): void {
    if (this.subs) {
      this.subs.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.subs.add(
      this.financeService.getData().subscribe(
        (res) => {
          this.size = Object.keys(res).length; //se obtiene el tamaño del Array para saber cuantos catalogos existen
          this.dataArray = res;
          const result = Object.keys(this.dataArray).map(
            (e) => this.dataArray[e]
          );
          this.datitos = this.dataArray;           
        },
        (err: HttpErrorResponse) => {
          console.log(err);
        }
      )
    );
  }  
}
