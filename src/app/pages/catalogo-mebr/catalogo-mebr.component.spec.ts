import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogoMEBRComponent } from './catalogo-mebr.component';

describe('CatalogoMEBRComponent', () => {
  let component: CatalogoMEBRComponent;
  let fixture: ComponentFixture<CatalogoMEBRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CatalogoMEBRComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogoMEBRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
