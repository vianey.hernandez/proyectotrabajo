import { CatalogoMEBRComponent } from './pages/catalogo-mebr/catalogo-mebr.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; 
import { TableComponent } from './components/table/table.component';

const routes: Routes = [
  {path:'', component:HomePageComponent},
  {path:'catalogos', component: CatalogoMEBRComponent},
  {path:'catalogo/:id', component: TableComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
