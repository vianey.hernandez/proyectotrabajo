import { PopUpComponent } from './../pop-up/pop-up.component';
import { DataService } from './../../services/data.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.sass'],
})
export class TableComponent implements OnInit {
  columnas: any[] = []; //guardar las columnas
  dataArray: any; //variable auxiliar
  displayedColumns: any[] = [];
  aux: any[] = [];
  //elementos para mostrar la tabla
  public dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  constructor(
    private consultarDatos: DataService,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {}

  Obtenerdatos(id: any): void {
    this.consultarDatos.getCatalogo(id).subscribe(
      (res) => {
        this.dataArray = res;
        this.columnas = this.dataArray.columnas;
        this.dataSource = new MatTableDataSource<any>(this.dataArray.celdas);
        this.dataSource.paginator = this.paginator;
        this.displayedColumns = this.columnas.map((c) => c.id);
      },
      (err: HttpErrorResponse) => {
        console.log(err);
      }
    );
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.Obtenerdatos(params['id']);
    });
  }
  //Empieza funcionamiento de ventana pop-up
  id_pais!: string;
  cve_pais!: string;
  desc_pais_original!: string;
  desc_pais!: string;
  ind_indice!: string;

  openDialog(): void {

    const dialogRef = this.dialog.open(PopUpComponent, {
      width: '250px',
      data: {
        id_pais: this.id_pais,
        cve_pais: this.cve_pais,
        desc_pais_original: this.desc_pais_original,
        desc_pais: this.desc_pais,
        ind_indice: this.ind_indice,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('Dialog output:', result); 
      this.dataArray.celdas.push(result);
      console.log(this.dataArray.celdas);
      this.dataSource.paginator = this.paginator;

    });
  }
}
