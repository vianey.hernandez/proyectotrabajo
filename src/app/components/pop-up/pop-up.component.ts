import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.sass'],
})
export class PopUpComponent implements OnInit {
  form: FormGroup;
  
  constructor(
    public dialogRef: MatDialogRef<PopUpComponent>,
    private fb: FormBuilder,

    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.form = fb.group({
      id_pais: data.id_pais,
      cve_pais: data.cve_pais,
      desc_pais_original: data.desc_pais_original,
      desc_pais: data.desc_pais,
      ind_indice: data.ind_indice
    });
  }

  ngOnInit(): void {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.form.value);
  }
}
 